from django.urls import path
from tasks.views import create_task, show_my_tasks, create_notes

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path('task/<int:task_id>/create_notes/', create_notes, name="create_notes"),
]
