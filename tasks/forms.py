from tasks.models import Task, Note
from django import forms
from datetime import date


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]


class NotesForm(forms.ModelForm):
    created_at = forms.DateField(initial=date.today, widget=forms.HiddenInput())
    class Meta:
        model = Note
        exclude = ['created_at']
        fields = [
            "text",
        ]
