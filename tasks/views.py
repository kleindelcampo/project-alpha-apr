from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, NotesForm
from tasks.models import Task, Note
from datetime import datetime
# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm

    context = {
        "form": form
    }
    return render(request, "task/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "task/show_my_tasks.html", context)

@login_required
def create_notes(request, task_id):
    task = Task.objects.get(id=task_id)
    if request.method == "POST":
        form = NotesForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']
            created_at = datetime.now()
            new_note = Note.objects.create(task=task, text=text, created_at=created_at)
            return redirect("show_my_tasks")
    else:
        form = NotesForm
    context = {
        "form": form,
        "task": task,
    }
    return render(request, "task/create_notes.html", context)
